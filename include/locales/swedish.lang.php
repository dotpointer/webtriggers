<?php

# webtriggers translation file for Swedish

# changelog
# 2021-11-07 03:32:00
# 2022-12-03 20:23:00 - abort

# store this into translations
$translations['languages'][] = array(
  'locales' => array(
    'sv-SE'
  ),
  'content' => array(
    array('Abort', 'Avbryt'),
    array('Aborted', 'Avbruten'),
    array('Actions', 'Händelser'),
    array('Created', 'Skapad'),
    array('End manually', 'Avsluta manuellt'),
    array('End', 'Slut'),
    array('Ended manually', 'Manuellt avslutad'),
    array('Ended', 'Avslutad'),
    array('Error, bad exit code', 'Fel, dåligt returvärde'),
    array('Error, not found in configuration file', 'Fel, kunde inte hittas i konfigurationsfilen'),
    array('Manage', 'Hantera'),
    array('Name', 'Namn'),
    array('Please confirm the following action', 'Vänligen bekräfta följande åtgärd'),
    array('Output', 'Utdata'),
    array('Queue - latest', 'Kö - senaste'),
    array('Queued action', 'Köade händelse'),
    array('Queued', 'Köad'),
    array('Return code', 'Returvärde'),
    array('Run', 'Kör'),
    array('Start', 'Start'),
    array('Started', 'Påbörjad'),
    array('This will set the order as manually ended. If the process is still running then it will not be ended. Do you want to continue?', 'Detta kommer markera ordern som manuellt avslutad. Om processen fortfarande körs kommer den inte avslutas. Vill du fortsätta?')
  )
);
?>
