<?php
# webtriggers setup fill in this file and rename it to setup.php

#  changelog
#  2021-11-06 18:31:00
#  2023-02-11 17:03:00

# configuration file - must exist
define('CONFIGFILE', '/etc/dptools/webtriggers');

# database setup, fill this in for web based orders:
define('DATABASE_HOST', 'localhost');
define('DATABASE_USERNAME', 'www');
define('DATABASE_PASSWORD', 'www');

# database setup, fill in database name or set to false to 
# disable web based orders:
define('DATABASE_NAME', 'webtriggers');

# not implemented
# define('DATABASE_TABLES_PREFIX', '' /* 'webtriggers_'*/);

# log file location, set to false to disable
define('LOGFILE', '/var/log/webtriggers');

# order files path, set to false to disable
define('ORDER_FILES_PATH', false);
#define('ORDER_FILES_PATH', '/tmp/');

# trigger file - must exist
define('TRIGGERFILE', '/var/cache/webtriggers.trigger');

# web server username to be granted access to trigger file
define('USERNAME_HTTP', 'www-data');

# verbosity levels for command line interface and log file
define('VERBOSITY_CLI', VERBOSE_ERROR);
define('VERBOSITY_LOGFILE', VERBOSE_INFO);

# enable web interface, set to false to disable
define('WEB_ENABLED', true);

?>
